package com.maragues.adidas.fit;

/**
 * Created by miguelaragues on 8/6/17.
 */

public class FitManagerException extends Throwable {
  FitManagerException(String message){
    super(message);
  }
}

package com.maragues.adidas.test.dagger;

import com.maragues.adidas.model.AppDatabase;
import com.maragues.adidas.model.ChallengeDao;
import com.maragues.adidas.model.UserChallengesDao;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

import static org.mockito.Mockito.mock;

/**
 * Created by miguelaragues on 8/6/17.
 */
@Module class UnitTestDatabaseModule {

  @Provides
  @Singleton
  ChallengeDao providesChallengeDao() {
    return mock(ChallengeDao.class);
  }

  @Provides
  @Singleton
  UserChallengesDao providesUserChallengesDao() {
    return mock(UserChallengesDao.class);
  }
}

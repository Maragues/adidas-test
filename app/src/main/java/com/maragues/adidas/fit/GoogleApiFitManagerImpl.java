package com.maragues.adidas.fit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.maragues.adidas.App;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import java.util.concurrent.TimeUnit;

/**
 * Fit manager that knows how to fetch fitness data from GoogleApiClient
 *
 * Created by miguelaragues on 8/6/17.
 */

class GoogleApiFitManagerImpl implements FitManager {
  private static final String TAG = GoogleApiFitManagerImpl.class.getSimpleName();

  private final GoogleApiClient googleApiClient;

  private final PublishSubject<ConnectionStatus> connectionPublishSubject = PublishSubject.create();

  GoogleApiFitManagerImpl(GoogleApiClient googleApiClient) {
    this.googleApiClient = googleApiClient;
  }

  @Override public PublishSubject<ConnectionStatus> connectionStatusObservable() {
    return connectionPublishSubject;
  }

  /**
   * Record step data by requesting a subscription to background step data.
   */
  Observable<Boolean> subscribeToDataType(DataType dataType) {
    return Observable.create(e -> {
      // To create a subscription, invoke the Recording API. As soon as the subscription is
      // active, fitness data will start recording.
      Fitness.RecordingApi.subscribe(googleApiClient, dataType)
          .setResultCallback(status -> {
            if (status.isSuccess()) {
              if (status.getStatusCode()
                  == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                Log.i(TAG, "Existing subscription for activity detected.");
              } else {
                Log.i(TAG, "Successfully subscribed!");
              }
            } else {
              Log.w(TAG, "There was a problem subscribing: " + status.getStatusCode());
            }

            e.onNext(status.isSuccess());
            e.onComplete();
          });
    });
  }

  @Override
  public Single<Long> getDailyStepsCount() {
    Log.d(TAG, "Reading daily steps");
    DailyTotalResult dailyTotalResult = Fitness.HistoryApi
        .readDailyTotal(googleApiClient, DataType.TYPE_STEP_COUNT_DELTA)
        .await(30, TimeUnit.SECONDS);
    if (dailyTotalResult.getStatus().isSuccess()) {
      DataSet totalSet = dailyTotalResult.getTotal();
      long total = totalSet.isEmpty()
          ? 0
          : totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();

      return Single.just(total);
    }

    return Single.error(new FitManagerException("Unable to read daily step count"));
  }

  @Override public Observable<Boolean> subscribeToSteps() {
    Log.d(TAG, "subscribeToSteps is connected " + googleApiClient.isConnected());
    return Observable.just(googleApiClient.blockingConnect(3, TimeUnit.SECONDS))
        .flatMap(new Function<ConnectionResult, ObservableSource<Boolean>>() {
          @Override
          public ObservableSource<Boolean> apply(@NonNull ConnectionResult connectionResult)
              throws Exception {
            Log.d(TAG, "blockingConnect success " + connectionResult.isSuccess());
            if (connectionResult.isSuccess()) {
              return subscribeToDataType(DataType.TYPE_STEP_COUNT_DELTA);
            }

            return Observable.just(false);
          }
        });
  }
}

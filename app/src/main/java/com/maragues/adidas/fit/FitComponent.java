package com.maragues.adidas.fit;

import android.content.Context;
import com.google.android.gms.common.api.GoogleApiClient;
import com.maragues.adidas.dagger.ActivityScope;
import com.maragues.adidas.dagger.AppComponent;
import com.maragues.adidas.model.DatabaseModule;
import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by miguelaragues on 8/6/17.
 */
@ActivityScope
@Component(
    modules = {
        FitModule.class
    }
)
public interface FitComponent {
  FitManager fitManager();

  @Component.Builder
  interface Builder {
    @BindsInstance Builder googleApiClient(GoogleApiClient googleApiClient);
    FitComponent build();
  }
}

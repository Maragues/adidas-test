package com.maragues.adidas.ui.challenge;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;

/**
 * This class is responsible to drive the ChallengeView.
 * Internally this creates a {@link ChallengeView} and attaches it to the {@link ChallengePresenter}
 * and offers public API to fire view intents and to check for expected view.render() events.
 *
 * <p>
 * <b>Create a new instance for every unit test</b>
 * </p>
 *
 * Created by miguelaragues on 28/5/17.
 */

class ChallengeViewRobot {
  private final PublishSubject<Boolean> fabSubject = PublishSubject.create();
  private final List<ChallengeViewState> renderEvents = new CopyOnWriteArrayList<>();
  private final ReplaySubject<ChallengeViewState> renderEventSubject = ReplaySubject.create();
  private final ChallengePresenter presenter;

  private ChallengeView view = new ChallengeView() {
    @Override public Observable<Boolean> toggleSubscriptionIntent() {
      return fabSubject;
    }

    @Override public void render(ChallengeViewState viewState) {
      renderEvents.add(viewState);
      renderEventSubject.onNext(viewState);
    }
  };

  ChallengeViewRobot(ChallengePresenter presenter) {
    this.presenter = presenter;
    presenter.attachView(view);
  }

  void fireToggleSubscriptionIntent() {
    fabSubject.onNext(true);
  }

  /**
   * Blocking waits for view.render() calls and
   *
   * @param expectedHomeViewStates The expected  HomeViewStates that will be passed to
   * view.render()
   */
  public void assertViewStateRendered(ChallengeViewState... expectedHomeViewStates) {

    if (expectedHomeViewStates == null) {
      throw new NullPointerException("expectedHomeViewStates == null");
    }

    int eventsCount = expectedHomeViewStates.length;
    renderEventSubject.take(eventsCount)
        .timeout(10, TimeUnit.SECONDS)
        .blockingSubscribe();

    if (renderEventSubject.getValues().length > eventsCount) {
      Assert.fail("Expected to wait for "
          + eventsCount
          + ", but there were "
          + renderEventSubject.getValues().length
          + " Events in total, which is more than expected: "
          + arrayToString(renderEventSubject.getValues()));
    }

    Assert.assertEquals(Arrays.asList(expectedHomeViewStates), renderEvents);
  }

  /**
   * Simple helper function to print the content of an array as a string
   */
  private String arrayToString(Object[] array) {
    StringBuffer buffer = new StringBuffer();
    for (Object o : array) {
      buffer.append(o.toString());
      buffer.append("\n");
    }

    return buffer.toString();
  }
}

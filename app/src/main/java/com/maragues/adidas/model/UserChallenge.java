package com.maragues.adidas.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import static com.maragues.adidas.model.UserChallenge.USERNAME;

/**
 * User's subscription state to a Challenge
 *
 * Representation of a M:N relationship between Users and Challenges
 *
 * Created by miguelaragues on 7/6/17.
 */
@Entity(
    tableName = "user_challenges",
    primaryKeys = { UserChallenge.CHALLENGE_ID, USERNAME }
)
public class UserChallenge {

  static final String CHALLENGE_ID = "challenge_id";
  static final String USERNAME = "username";

  @ColumnInfo(name = CHALLENGE_ID)
  private long challengeId;

  @ColumnInfo(name = USERNAME)
  private String username;

  private boolean subscribed;

  public long getChallengeId() {
    return challengeId;
  }

  public void setChallengeId(long challengeId) {
    this.challengeId = challengeId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public boolean isSubscribed() {
    return subscribed;
  }

  public void setSubscribed(boolean subscribed) {
    this.subscribed = subscribed;
  }

  public static UserChallenge createFromChallenge(long challengeId) {
    UserChallenge userChallenge = new UserChallenge();

    userChallenge.challengeId = challengeId;
    userChallenge.username = "Miguel";

    return userChallenge;
  }
}

package com.maragues.adidas.ui.challenge;

import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.hannesdorfmann.mosby3.mvi.MviActivity;
import com.jakewharton.rxbinding2.view.RxView;
import com.maragues.adidas.R;
import io.reactivex.Observable;

public class ChallengeActivity extends MviActivity<ChallengeView, ChallengePresenter>
    implements ChallengeView {
  private static final String TAG = ChallengeActivity.class.getSimpleName();

  private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 12;

  @BindView(R.id.fab) FloatingActionButton fab;
  @BindView(R.id.steps_taken) TextView stepsTakenView;

  private ChallengeComponent challengeComponent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_challenge);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    ButterKnife.bind(this);

    challengeComponent = DaggerChallengeComponent.builder()
        .googleApiClient(buildFitnessClient())
        .build();
  }

  /**
   * Build a {@link GoogleApiClient} to authenticate the user and allow the application
   * to connect to the Fitness APIs. The included scopes should match the scopes needed
   * by your app (see the documentation for details).
   * Use the {@link GoogleApiClient.OnConnectionFailedListener}
   * to resolve authentication failures (for example, the user has not signed in
   * before, or has multiple accounts and must specify which account to use).
   */
  private GoogleApiClient buildFitnessClient() {
    // Create the Google API Client
    return new GoogleApiClient.Builder(this)
        .addApi(Fitness.RECORDING_API)
        .addApi(Fitness.HISTORY_API)
        .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
        .addConnectionCallbacks(
            new GoogleApiClient.ConnectionCallbacks() {

              @Override
              public void onConnected(Bundle bundle) {
                Log.i(TAG, "Connected!!!");
                // Now you can make calls to the Fitness APIs.  What to do?
              }

              @Override
              public void onConnectionSuspended(int i) {
                // If your connection to the sensor gets lost at some point,
                // you'll be able to determine the reason and react to it here.
                if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                  Log.w(TAG, "Connection lost.  Cause: Network Lost.");
                } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                  Log.w(TAG, "Connection lost.  Reason: Service Disconnected");
                }
              }
            }
        )
        .enableAutoManage(this, 0, new GoogleApiClient.OnConnectionFailedListener() {
          @Override
          public void onConnectionFailed(ConnectionResult result) {
            Log.w(TAG, "Google Play services connection failed. Cause: " +
                result.toString());
          }
        })
        .build();
  }

  @NonNull @Override public ChallengePresenter createPresenter() {
    return challengeComponent.challengePresenter();
  }

  @Override public Observable<Boolean> toggleSubscriptionIntent() {
    return RxView.clicks(fab)
        .map(ignored -> true);
  }

  @Override public void render(ChallengeViewState viewState) {
    Log.d(TAG,"Rendering, subcribed "+viewState.subscribed());
    renderLoading(viewState);

    renderSubscribed(viewState);
  }

  private void renderLoading(ChallengeViewState viewState) {
    if (viewState.loading()) {
      final OvershootInterpolator interpolator = new OvershootInterpolator();
      ViewCompat.animate(fab)
          .rotation(360f)
          .withLayer()
          .setDuration(300)
          .setInterpolator(interpolator)
          .start();
    } else {
      fab.clearAnimation();
    }
  }

  private void renderSubscribed(ChallengeViewState viewState) {
    @ColorRes int color;
    if (viewState.subscribed()) {
      color = R.color.colorPrimary;

      stepsTakenView.setVisibility(View.VISIBLE);
      stepsTakenView.setText(getString(R.string.steps_counter, String.valueOf(viewState.steps())));
    } else {
      color = android.R.color.white;

      stepsTakenView.setVisibility(View.GONE);
      stepsTakenView.setText("");
    }

    fab.getDrawable().mutate().setTint(ContextCompat.getColor(this, color));
  }
}

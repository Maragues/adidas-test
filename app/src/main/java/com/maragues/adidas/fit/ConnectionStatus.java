package com.maragues.adidas.fit;

import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.auto.value.AutoValue;

/**
 * Created by miguelaragues on 8/6/17.
 */

@AutoValue
public abstract class ConnectionStatus {
  public abstract boolean success();

  public abstract int suspendedCause();

  @Nullable
  public abstract ConnectionResult failureResult();

  static ConnectionStatus onConnected() {
    return new AutoValue_ConnectionStatus(true, 0, null);
  }

  static ConnectionStatus onSuspended(int suspensionCause) {
    return new AutoValue_ConnectionStatus(false, suspensionCause, null);
  }

  static ConnectionStatus onFailed(ConnectionResult result) {
    return new AutoValue_ConnectionStatus(false, 0, result);
  }
}

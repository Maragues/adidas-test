package com.maragues.adidas.fit;

import android.app.Activity;
import com.google.android.gms.common.api.GoogleApiClient;
import dagger.Module;
import dagger.Provides;
import java.lang.ref.WeakReference;
import javax.inject.Singleton;

/**
 * Created by miguelaragues on 8/6/17.
 */
@Module public class FitModule {

  @Provides
  FitManager providesFitManager(GoogleApiClient googleApiClient) {
    return new GoogleApiFitManagerImpl(googleApiClient);
  }
}

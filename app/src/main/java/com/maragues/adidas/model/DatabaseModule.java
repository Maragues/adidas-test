package com.maragues.adidas.model;

import android.arch.persistence.room.Room;
import android.content.Context;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Created by miguelaragues on 7/6/17.
 */
@Module
@Singleton
public class DatabaseModule {
  @Provides
  @Singleton
  AppDatabase providesAppDatabase(Context context) {
    return Room.databaseBuilder(context, AppDatabase.class, "adidas-db").build();
  }

  @Provides
  ChallengeDao providesChallengeDao(AppDatabase appDatabase) {
    return appDatabase.challengeDao();
  }

  @Provides
  UserChallengesDao providesUserChallengesDao(AppDatabase appDatabase) {
    return appDatabase.userChallengeDao();
  }
}

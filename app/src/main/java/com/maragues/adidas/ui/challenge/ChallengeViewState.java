package com.maragues.adidas.ui.challenge;

import com.google.auto.value.AutoValue;
import com.maragues.adidas.model.UserChallenge;

/**
 * Immutable class that represents all the possible states a ChallengeView can render
 *
 * Created by miguelaragues on 7/6/17.
 */
@AutoValue
abstract class ChallengeViewState {
  abstract boolean loading();

  abstract boolean subscribed();

  abstract long steps();

  static ChallengeViewState empty() {
    return new AutoValue_ChallengeViewState.Builder()
        .setLoading(false)
        .setSubscribed(false)
        .setSteps(0)
        .build();
  }

  abstract ChallengeViewState withLoading(boolean loading);

  abstract ChallengeViewState withSubscribed(boolean subscribed);

  abstract ChallengeViewState withSteps(long steps);

  static ChallengeViewState fromUserChallenge(UserChallenge userChallenge) {
    return empty()
        .withSubscribed(userChallenge.isSubscribed());
  }

  @AutoValue.Builder
  abstract static class Builder {
    public abstract Builder setLoading(boolean loading);

    public abstract Builder setSubscribed(boolean subscribed);

    public abstract Builder setSteps(long steps);

    abstract ChallengeViewState build();
  }
}

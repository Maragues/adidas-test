package com.maragues.adidas.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import io.reactivex.Flowable;
import java.util.List;

/**
 * Created by miguelaragues on 7/6/17.
 */
@Dao
public interface ChallengeDao {
  @Insert
  long insertChallenge(Challenge challenge);

  @Query("SELECT * from challenges")
  Flowable<List<Challenge>> getAll();

  @Query("SELECT * from challenges where id = :id LIMIT 1")
  Flowable<Challenge> loadChallengeById(long id);
}

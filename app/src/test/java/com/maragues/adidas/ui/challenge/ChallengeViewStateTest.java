package com.maragues.adidas.ui.challenge;

import com.maragues.adidas.test.BaseUnitTest;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by miguelaragues on 8/6/17.
 */
public class ChallengeViewStateTest extends BaseUnitTest {
  @Test
  public void empty_falseSubscribed_falseLoading_0Steps() {
    ChallengeViewState viewState = ChallengeViewState.empty();

    assertFalse(viewState.loading());
    assertFalse(viewState.subscribed());

    assertEquals(0, viewState.steps());
  }
}
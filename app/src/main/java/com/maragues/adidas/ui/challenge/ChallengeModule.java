package com.maragues.adidas.ui.challenge;

import com.maragues.adidas.dagger.ActivityScope;
import com.maragues.adidas.fit.FitManager;
import dagger.Module;
import dagger.Provides;

/**
 * Created by miguelaragues on 8/6/17.
 */
@Module
class ChallengeModule {
  @Provides
  @ActivityScope
  ChallengePresenter providesChallengePresenter(FitManager fitManager){
    return new ChallengePresenter(fitManager);
  }
}

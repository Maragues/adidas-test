package com.maragues.adidas.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by miguelaragues on 7/6/17.
 */
@Database(entities = { Challenge.class, UserChallenge.class }, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
  public abstract ChallengeDao challengeDao();
  public abstract UserChallengesDao userChallengeDao();
}

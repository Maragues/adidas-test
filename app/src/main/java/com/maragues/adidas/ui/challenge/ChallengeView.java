package com.maragues.adidas.ui.challenge;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import io.reactivex.Observable;

/**
 * Created by miguelaragues on 7/6/17.
 */

interface ChallengeView extends MvpView {
  Observable<Boolean> toggleSubscriptionIntent();

  /**
   * Renders the viewState
   */
  void render(ChallengeViewState viewState);
}

package com.maragues.adidas.fit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;

/**
 * This interface allows us to hide the complexity behind retrieving fitness data.
 *
 * By isolating the rest of the components from the fitness business logic, we could swap from
 * Google as a provider to a self-constructed solution, if we were to publish in a market different
 * than Google Play
 *
 * Created by miguelaragues on 8/6/17.
 */

public interface FitManager {
  PublishSubject<ConnectionStatus> connectionStatusObservable();

  Single<Long> getDailyStepsCount();

  Observable<Boolean> subscribeToSteps();
}

package com.maragues.adidas.ui.challenge;

import android.support.annotation.VisibleForTesting;
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter;
import com.maragues.adidas.App;
import com.maragues.adidas.fit.FitManager;
import com.maragues.adidas.model.Challenge;
import com.maragues.adidas.model.UserChallenge;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import java.util.List;
import org.reactivestreams.Publisher;

import static com.maragues.adidas.App.appComponent;

/**
 * Presenter responsible for handling intents expressed by ChallengeView and providing
 * ChallenveViewState for the view to render.
 *
 * Currently it only works for a step counting challenge, but with some work it could be abstracted
 * for any Challenge.
 *
 * Created by miguelaragues on 7/6/17.
 */

class ChallengePresenter extends MviBasePresenter<ChallengeView, ChallengeViewState> {
  private static final String TAG = ChallengePresenter.class.getSimpleName();
  private final FitManager fitManager;
  private long challengeId = 1;

  @VisibleForTesting
  UserChallenge userChallenge;

  final PublishSubject<ChallengeViewState> challengeViewStatePublishSubject
      = PublishSubject.create();

  ChallengePresenter(FitManager fitManager) {
    this.fitManager = fitManager;
  }

  @Override protected void bindIntents() {
    Observable<ChallengeViewState> subscribeClick = intent(ChallengeView::toggleSubscriptionIntent)
        .flatMap(ignore -> toggleSubscription());

    ChallengeViewState initialState = ChallengeViewState.empty();

    Observable<ChallengeViewState> allObservable = Observable.merge(
        challengeViewStatePublishSubject,
        subscribeClick);

    subscribeViewState(
        allObservable
            .startWith(initialState)
            /*
            If the ViewState is subscribed, fill the steps value
             */
            .concatMap(new Function<ChallengeViewState, ObservableSource<ChallengeViewState>>() {
              @Override
              public ObservableSource<ChallengeViewState> apply(
                  @NonNull ChallengeViewState challengeViewState)
                  throws Exception {
                if (challengeViewState.subscribed()) {
                  return createViewStateWithSteps(challengeViewState);
                }

                return Observable.just(challengeViewState);
              }
            })
            .observeOn(AndroidSchedulers.mainThread()),
        ChallengeView::render
    );

    loadChallenge();
  }

  /**
   * Loads Challenges from DB and creates 1 if there are none. Once it has a Challenge, it attempts
   * to load the corresponding UserChallenge. If there's none, it creates one.
   *
   * On the first run, it'll create a Challenge and a UserChallenge.
   *
   * From then on, it should use the stored records
   */
  void loadChallenge() {
    appComponent.challengeDao()
        .getAll()
        .subscribeOn(Schedulers.io())
        /*
         * load all challenges. If there aren't any, create 1.
         */
        .map(challenges -> {
          if (challenges.isEmpty()) {
            addChallenge(challenges);
          }

          return challenges;
        })
        /*
        Load UserChallenge from DB. If it doesn't exist, create it
         */
        .flatMap(new Function<List<Challenge>, Publisher<UserChallenge>>() {
          @Override public Publisher<UserChallenge> apply(@NonNull List<Challenge> challenges)
              throws Exception {
            return loadOrCreateUserChallenge(challenges);
          }
        })
        .subscribe(this::onUserChallengeLoaded, Throwable::printStackTrace);
  }

  /**
   * Attempts to load a UserChallenge. If it doesn't exist, it creates one
   *
   * @param challenges
   * @return
   */
  Publisher<UserChallenge> loadOrCreateUserChallenge(@NonNull List<Challenge> challenges) {
    UserChallenge userChallenge = App.appComponent.userChallengesDao()
        .loadByChallengeId(challenges.get(0).getId());

    if (userChallenge == null) {
      userChallenge = UserChallenge.createFromChallenge(challenges.get(0).getId());

      appComponent.userChallengesDao().insertUserChallenge(userChallenge);
    }

    return Flowable.just(userChallenge);
  }

  /**
   * We've successfully loaded a user challenge, store the reference and emit a new ViewState
   *
   * Ideally we'd subscribe to some kind of LiveData or Observable that emits changes if the DB
   * record is updates
   * @param userChallenge
   */
  void onUserChallengeLoaded(UserChallenge userChallenge) {
    this.userChallenge = userChallenge;

    challengeViewStatePublishSubject.onNext(ChallengeViewState.fromUserChallenge(userChallenge));
  }

  void addChallenge(@NonNull List<Challenge> challenges) {
    Challenge challenge = Challenge.create("Steps");
    challenge.setId(challengeId);
    appComponent.challengeDao().insertChallenge(challenge);

    challenges.add(challenge);
  }

  /**
   * Toggles user's subcription to the challenge
   *
   * @return an observable emitting ChallengeViewState representing the new view state
   */
  @VisibleForTesting
  Observable<ChallengeViewState> toggleSubscription() {
    return Observable.just(userChallenge)
        .subscribeOn(Schedulers.io())
        .flatMap(new Function<UserChallenge, ObservableSource<ChallengeViewState>>() {
          @Override
          public ObservableSource<ChallengeViewState> apply(
              @NonNull final UserChallenge userChallenge)
              throws Exception {
            final boolean previousSubscribedState = userChallenge.isSubscribed();

            userChallenge.setSubscribed(!userChallenge.isSubscribed());

            return updateUserChallenge(userChallenge, previousSubscribedState);
          }
        })
        .startWith(ChallengeViewState.empty().withLoading(true));
  }

  /**
   * Updates UserChallenge record and returns a ChallengeViewState describing the subscription
   * status and the steps, if it applies
   */
  Observable<ChallengeViewState> updateUserChallenge(
      @NonNull final UserChallenge userChallenge,
      final boolean previousSubscribedState) {
    return Observable.just(
        appComponent.userChallengesDao().updateUserChallenge(userChallenge))
        .flatMap(new Function<Integer, ObservableSource<ChallengeViewState>>() {
          @Override
          public ObservableSource<ChallengeViewState> apply(@NonNull Integer rowsUpdated)
              throws Exception {
            boolean subscribed = rowsUpdated > 0
                ? userChallenge.isSubscribed()
                : previousSubscribedState;

            return Observable.just(ChallengeViewState.empty()
                .withSubscribed(subscribed)
                .withLoading(false));
          }
        });
  }

  /**
   * Given a ChallengeViewState, fill the number of steps the user has done since midnight,
   * @param challengeViewState
   * @return
   */
  ObservableSource<ChallengeViewState> createViewStateWithSteps(
      @NonNull final ChallengeViewState challengeViewState) {
    return stepsCount()
        .flatMap(
            new Function<Long, ObservableSource<ChallengeViewState>>() {
              @Override
              public ObservableSource<ChallengeViewState> apply(
                  @NonNull Long steps)
                  throws Exception {
                return Observable.just(
                    challengeViewState.withSteps(steps)
                );
              }
            });
  }

  /**
   * Requests the step count from FitManager
   *
   * @return
   */
  Observable<Long> stepsCount() {
    return fitManager.subscribeToSteps()
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .flatMapSingle(new Function<Boolean, SingleSource<Long>>() {
          @Override public SingleSource<Long> apply(@NonNull Boolean subscriptionResult)
              throws Exception {
            if (subscriptionResult) {
              return fitManager.getDailyStepsCount();
            }
            return Single.just(0L);
          }
        });
  }
}

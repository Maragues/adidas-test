package com.maragues.adidas;

import android.app.Application;
import com.maragues.adidas.dagger.AppComponent;
import com.maragues.adidas.dagger.DaggerAppComponent;

/**
 * Created by miguelaragues on 7/6/17.
 */

public class App extends Application {

  public static AppComponent appComponent;

  @Override public void onCreate() {
    super.onCreate();

    initDagger();
  }

  private void initDagger() {
    appComponent = DaggerAppComponent.builder()
        .context(this)
        .build();
  }
}

package com.maragues.adidas.ui.challenge;

import com.maragues.adidas.App;
import com.maragues.adidas.fit.FitManager;
import com.maragues.adidas.model.UserChallenge;
import com.maragues.adidas.test.BaseUnitTest;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by miguelaragues on 8/6/17.
 */
public class ChallengePresenterTest extends BaseUnitTest {
  ChallengePresenter challengePresenter;

  /*
  We should be working with a mock here, not a real object. We shouldn't care/know about the
  internals of this object, and we are relying on the setters/getters, which is wrong
   */
  UserChallenge userChallenge;

  @Mock FitManager fitManager;

  public void setup() throws Exception {
    super.setup();

    userChallenge = new UserChallenge();

    challengePresenter = spy(new ChallengePresenter(fitManager));

    challengePresenter.userChallenge = userChallenge;
  }

  @Test
  public void render_sendsInitialViewStateEmpty(){
    doNothing().when(challengePresenter).loadChallenge();

    ChallengeViewState expectedInitialState = ChallengeViewState.empty();

    ChallengeViewRobot challengeViewRobot = new ChallengeViewRobot(challengePresenter);

    challengeViewRobot.assertViewStateRendered(expectedInitialState);
  }

  /*
  TOGGLE SUBSCRIPTION
   */

  @SuppressWarnings("ConstantConditions")
  @Test
  public void toggleSubscription_subscribedFalse_invokesUpdateUserChallengeWithSubscribedTrue() {
    boolean initialState = false;
    userChallenge.setSubscribed(initialState);

    doNothing().when(challengePresenter).loadChallenge();

    ChallengeViewRobot challengeViewRobot = new ChallengeViewRobot(challengePresenter);

    doReturn(Observable.empty()).when(challengePresenter)
        .updateUserChallenge(any(UserChallenge.class), anyBoolean());

    challengeViewRobot.fireToggleSubscriptionIntent();

    ArgumentCaptor<UserChallenge> challengeArgumentCaptor =
        ArgumentCaptor.forClass(UserChallenge.class);

    verify(challengePresenter).updateUserChallenge(challengeArgumentCaptor.capture(), eq(initialState));

    UserChallenge userChallenge = challengeArgumentCaptor.getValue();

    assertEquals(!initialState, userChallenge.isSubscribed());
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void toggleSubscription_subscribedTrue_invokesUpdateUserChallengeWithSubscribedFalse() {
    boolean initialState = true;
    userChallenge.setSubscribed(initialState);

    doNothing().when(challengePresenter).loadChallenge();

    ChallengeViewRobot challengeViewRobot = new ChallengeViewRobot(challengePresenter);

    doReturn(Observable.empty()).when(challengePresenter)
        .updateUserChallenge(any(UserChallenge.class), anyBoolean());

    challengeViewRobot.fireToggleSubscriptionIntent();

    ArgumentCaptor<UserChallenge> challengeArgumentCaptor =
        ArgumentCaptor.forClass(UserChallenge.class);

    verify(challengePresenter).updateUserChallenge(challengeArgumentCaptor.capture(), eq(initialState));

    UserChallenge userChallenge = challengeArgumentCaptor.getValue();

    assertEquals(!initialState, userChallenge.isSubscribed());
  }

  /*
  UPDATE USER CHALLENGE
   */

  @Test
  public void updateUserChallenge_invokesUserChallengeDao(){
    challengePresenter.updateUserChallenge(userChallenge, true).test();

    verify(App.appComponent.userChallengesDao()).updateUserChallenge(eq(userChallenge));
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void updateUserChallenge_0RowsUpdated_returnsViewStateWithPreviousState(){
    boolean previousState = true;

    when(App.appComponent.userChallengesDao().updateUserChallenge(any(UserChallenge.class)))
        .thenReturn(0);

    userChallenge.setSubscribed(!previousState);

    TestObserver<ChallengeViewState> observer =
        challengePresenter.updateUserChallenge(userChallenge, previousState).test();

    observer.assertValueCount(1);

    assertEquals(previousState, observer.values().get(0).subscribed());
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void updateUserChallenge_0RowsUpdated_returnsViewStateWithLoadingFalse(){
    boolean ignored = true;

    when(App.appComponent.userChallengesDao().updateUserChallenge(any(UserChallenge.class)))
        .thenReturn(0);

    TestObserver<ChallengeViewState> observer =
        challengePresenter.updateUserChallenge(userChallenge, ignored).test();

    observer.assertValueCount(1);

    assertFalse(observer.values().get(0).loading());
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void updateUserChallenge_1RowUpdated_returnsViewStateWithPassedState(){
    boolean previousState = true;

    when(App.appComponent.userChallengesDao().updateUserChallenge(any(UserChallenge.class)))
        .thenReturn(1);

    userChallenge.setSubscribed(!previousState);

    TestObserver<ChallengeViewState> observer =
        challengePresenter.updateUserChallenge(userChallenge, previousState).test();

    observer.assertValueCount(1);

    assertEquals(!previousState, observer.values().get(0).subscribed());
  }

  @SuppressWarnings("ConstantConditions")
  @Test
  public void updateUserChallenge_1RowsUpdated_returnsViewStateWithLoadingFalse(){
    boolean ignored = true;

    when(App.appComponent.userChallengesDao().updateUserChallenge(any(UserChallenge.class)))
        .thenReturn(1);

    TestObserver<ChallengeViewState> observer =
        challengePresenter.updateUserChallenge(userChallenge, ignored).test();

    observer.assertValueCount(1);

    assertFalse(observer.values().get(0).loading());
  }

  /*
  TODO add dozens of tests
   */
}
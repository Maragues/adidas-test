package com.maragues.adidas.test;

import com.maragues.adidas.App;
import com.maragues.adidas.dagger.DaggerAppComponent;
import com.maragues.adidas.test.rules.ImmediateRxSchedulersOverrideRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

/**
 * Created by miguelaragues on 28/5/17.
 */

public abstract class BaseUnitTest {

  @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

  @Rule
  public final ImmediateRxSchedulersOverrideRule mOverrideSchedulersRule =
      new ImmediateRxSchedulersOverrideRule();

  @Before public void setup() throws Exception {
    createAppComponent();
  }

  @After public void tearDown() throws Exception {
  }

  private void createAppComponent() {
    //the compiler may complain but the tests run
    App.appComponent = com.maragues.adidas.test.dagger.DaggerUnitTestAppComponent.create();
  }
}

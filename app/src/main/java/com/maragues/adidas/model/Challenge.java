package com.maragues.adidas.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import java.util.List;

/**
 * Represents a Challenge. Initially I made it immutable, but Room doesn't support AutoValue yet.
 *
 * Created by miguelaragues on 7/6/17.
 */
@Entity(tableName = "challenges")
public class Challenge {
  @PrimaryKey(autoGenerate = true)
  private long id;

  @ColumnInfo(name = "name")
  private String name;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public static Challenge create(String name) {
    Challenge challenge = new Challenge();
    challenge.name = name;
    return challenge;
  }
}

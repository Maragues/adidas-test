package com.maragues.adidas.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import io.reactivex.Flowable;
import java.util.List;

/**
 * Created by miguelaragues on 7/6/17.
 */
@Dao
public interface UserChallengesDao {
  @Insert
  void insertUserChallenge(UserChallenge userChallenge);

  @Update
  int updateUserChallenge(UserChallenge userChallenge);

  @Query("SELECT * from user_challenges")
  Flowable<List<UserChallenge>> getAll();

  @Query("SELECT * from user_challenges where challenge_id = :challengeId LIMIT 1")
  UserChallenge loadByChallengeId(long challengeId);
}

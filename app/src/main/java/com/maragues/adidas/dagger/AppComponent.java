package com.maragues.adidas.dagger;

import android.content.Context;
import com.maragues.adidas.fit.FitManager;
import com.maragues.adidas.fit.FitModule;
import com.maragues.adidas.model.ChallengeDao;
import com.maragues.adidas.model.DatabaseModule;
import com.maragues.adidas.model.UserChallengesDao;
import dagger.BindsInstance;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Single component for this app, but ideally it should be architected with scopes, submodules, etc.
 *
 * Created by miguelaragues on 7/6/17.
 */
@Singleton
@Component(
    modules = {
        AppModule.class,
        DatabaseModule.class
    }
)
public interface AppComponent {
  Context context();

  ChallengeDao challengeDao();
  UserChallengesDao userChallengesDao();

  @Component.Builder
  interface Builder {
    @BindsInstance Builder context(Context context);
    AppComponent build();
  }

}

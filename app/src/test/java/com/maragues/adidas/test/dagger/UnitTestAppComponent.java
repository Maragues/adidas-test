package com.maragues.adidas.test.dagger;

import com.maragues.adidas.dagger.AppComponent;
import dagger.Component;
import dagger.Module;
import javax.inject.Singleton;

/**
 * Created by miguelaragues on 8/6/17.
 */
@Singleton
@Component(
    modules = {
        UnitTestAppModule.class,
        UnitTestDatabaseModule.class
    }
)
public interface UnitTestAppComponent extends AppComponent{
}

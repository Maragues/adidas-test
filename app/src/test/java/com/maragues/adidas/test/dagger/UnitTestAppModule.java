package com.maragues.adidas.test.dagger;

import android.content.Context;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

import static org.mockito.Mockito.mock;

/**
 * Created by miguelaragues on 8/6/17.
 */
@Module class UnitTestAppModule {

  @Provides
  @Singleton
  public Context providesContext() {
    return mock(Context.class);
  }
}

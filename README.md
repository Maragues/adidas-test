The app reads the number of steps since midnight from Google Play Services' Fitness API.

I'm afraid I haven't had much time to work on it (~6 hours), since I could not start working until 
today and tomorrow early in the morning I'm leaving for a week. Since the deadline is on June 13, I
had to deliver whatever I could. I've focused on the backend, architecture and testing more than 
on the UI.

---


First of all you are asked for permissions by the app to access your fitness data. Ideally, this should 
be done lazily after you request to subscribe to the challenge.

Then, ChallengePresenter attempts to load challenges from the database. If it doesn't exist, it creates 
one. Then, it attemps to load the user's subscription state to the Challenge, and it also creates one 
if it doesn't exist. In a real world app, the data should come from a REST service.

The idea behind providing FitManager through Dependency Injection, which has caused me some headaches,
is to abstract the presentation layer from the implementation details of the data providers. Challenger presenter
only knows about FitManager, and if we decided to roll our own step counter solution or we had to work
in environments where Play Services isn't available, we could detect that at runtime and swap the 
FitManager without having to change a line of code in the View and Presenter layers.

The same idea works for persistence. By not tying ourselves to an implementation but to an
interface, we can easily swap our persistence provider as well as mock and test the interactions
with it.

That said, the app is tightly coupled to Goole Play Services since I init GoogleApiClient in Activity.
With more work this could be fixed. When I tried to init it in GoogleApiFitManagerImpl, it never 
asked for permissions, so even tho it requires a Context to be built, it should ask for an Activity.

From the visual point of view the app is terrible, I apologize.

Main features

- Use MVI architecture. I haven't worked a lot with it in the past, but I really like the idea of the
view rendering a single, immutable state. In the last years I've used extensively MVP, but I decided 
to try dig more into MVI for this exercise. 

- Used Room for SQLite persistence. I had never used it before, and I really liked it. 

- Fitness API. I would have liked to subscribe to step counter and update it live, but I am afraid
I can't devote more time. Also, there's a bug where if you launch the app with the screen off,
even if you are subscribed it doesn't read the steps. If you unsubscribe/subscribe, it works.

- JUnit test. I've added some junit tests, tho of course many are missing. Simple test of a method 
and a test following MVI principles 

- Missing Espresso tests. Espresso tests would be simpler with MVI because we could directly pass 
ChallengeViewState instances and check that the view has rendered them as expected

- The code quality isn't the best, but I've had to sacrifice it for development speed

Other projects you can take a look at

https://github.com/Maragues/MenuPlanner

I've some work in BitBucket which I could give you access to. It's another technical test I took last
week for an interview. I demonstrate the usage of Retrofit and how to mock interactions with a
REST service in JUnit tests.
package com.maragues.adidas.ui.challenge;

import com.google.android.gms.common.api.GoogleApiClient;
import com.maragues.adidas.dagger.ActivityScope;
import com.maragues.adidas.fit.FitModule;
import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by miguelaragues on 8/6/17.
 */
@ActivityScope
@Component(
    modules = {
        FitModule.class,
        ChallengeModule.class
    }
)
interface ChallengeComponent {

  ChallengePresenter challengePresenter();

  @Component.Builder
  interface Builder {
    @BindsInstance Builder googleApiClient(GoogleApiClient googleApiClient);
    ChallengeComponent build();
  }
}
